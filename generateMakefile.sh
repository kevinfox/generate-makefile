#!/bin/bash
# generateMakefile.sh
# Author: Kevin Fox
# Last modified: 2019 04 04

################
#### README ####
################

# This script auto-generates a makefile 
# There are a few variables that can be set:
#   - SRCDIR: The directory path, relative to this script, where the source files are stored 
#   - INCDIR: The directory path, relative to this script, where the header files are stored
#   - BLDDIR: The directory path, relative to this script, where the build binaries should be placed
#   - ARCH: The target architecture, if wanting multiple builds
#   - MAKEFILE: The name of this makefile
#   - PROJECT: The name of the project
#   - SRCTYPE: The source extension, such as .cpp or .cc
#   - INCTYPE: The header extension, such as .hpp or .h

###################
#### VARIABLES ####
###################

SRCDIR="source"
INCDIR="include"
BLDDIR="build"
ARCH="linux64"
MAKEFILE="makefile"
PROJECT="sbdReporting"
SRCTYPE=".cpp"
INCTYPE=".hpp"

########################################
#### DO NOT EDIT BEYOND THIS POINT* ####
########################################
# *unless you're confident with bash and makefiles

# Initial file creation
touch $MAKEFILE

# Getting total file count 
SRCNUM=$(ls $SRCDIR | wc -l)

# Getting source file names
SRCFILES=($(ls $SRCDIR | grep $SRCTYPE))

# Generating object file names
declare -a OBJFILES=()
index=0
# Replace <srcFile>.<srcType> with <srcFile>.o
for name in "${SRCFILES[@]}"; do
	editname=${name%$SRCTYPE}
	editname="$editname.o"
	OBJFILES[$index]="$editname" 
	(( index=$index+1 ))
done

# Create first half of makefile
echo "\
# makefile for $PROJECT
# Author: Kevin Fox
# Last Updated $(date -u +%F-%H%M)

SHELL = /bin/sh
.SUFFIXES:
.SUFFIXES: $SRCTYPE .o
CCO = g++ -o
CCC = g++ -c

BLDDIR := $BLDDIR
SRCDIR := $SRCDIR
INCDIR := $INCDIR
CFLAGS := -I \$(INCDIR)
TARDIR := \$(BLDDIR)/$ARCH
OBJDIR := \$(BLDDIR)/objects
TARGET := \$(TARDIR)/$PROJECT

OBJECTS := \$(addprefix \$(OBJDIR)/,${OBJFILES[@]})

\$(TARGET) : \$(OBJECTS)
  \$(CCO) \$(TARGET) \$(OBJECTS) \$(CFLAGS)
" >> $MAKEFILE

# Main loop to build out the makefile rules
index=0
# This loop goes through each source files
for (( index=0; index<$SRCNUM; index++ )); do
	declare -a localInc=()
	# Ignore lines that are #include <some standard lib> and that have comments "//" in them
	localInc=($(cat $SRCDIR/${SRCFILES[$index]} | grep "#include" | grep -v "<" | grep -v "//")) 
	incList=""
	incCount=${#localInc[@]}
	incIndex=0
	# This loop finds all of the #include "file.xxx" statements 
	for (( incIndex=0; incIndex<$incCount; incIndex++ )); do
		tempString=${localInc[$incIndex]}
		if [ "$tempString" != "#include" ]; then
			tempString=${tempString%\"}
			tempString=${tempString#\"}
			incList="$incList$tempString "
		fi
	done	
	# HERE IS WHERE WE WOULD CHECK HPP FILES - OR JUST ADD HEADERS TO SOURCE
	echo "  \
  Parsed ${SRCFILES[$index]}    
    Dependencies: $incList
  "
	echo "\
\$(OBJDIR)/${OBJFILES[$index]} : \$(SRCDIR)/${SRCFILES[$index]} \$(addprefix \$(INCDIR)/,$incList)
  \$(CCC) \$(SRCDIR)/${SRCFILES[$index]} -o \$(OBJDIR)/${OBJFILES[$index]} \$(CFLAGS)
" >> $MAKEFILE
done

# Final touches for makefile
echo "
.PHONY : clean

clean :
	rm -f \$(TARGET) \$(OBJECTS)

" >> $MAKEFILE

echo "
Initialized new makefile ... 

Checked how many source files ... 
  Source files: $SRCNUM

Got source file names ... 
  Source files in directory $SRCDIR/:
    ${SRCFILES[@]}

Generated object file names ... 
  Object files to be created in directory $BLDDIR/objects/: 
    ${OBJFILES[@]}

Generated rules for objects ...

Makefile generation complete!
"
