README

This script auto-generates a makefile 

There are a few variables that can be set:
* SRCDIR: The directory path, relative to this script, where the source files are stored 
* INCDIR: The directory path, relative to this script, where the header files are stored
* BLDDIR: The directory path, relative to this script, where the build binaries should be placed
* ARCH: The target architecture, if wanting multiple builds
* MAKEFILE: The name of this makefile
* PROJECT: The name of the project
* SRCTYPE: The source extension, such as .cpp or .cc
* INCTYPE: The header extension, such as .hpp or .h
